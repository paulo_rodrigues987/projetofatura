import { FaturaListComponent } from './fatura-list/fatura-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaturaFormComponent } from './fatura-form/fatura-form.component';

const routes: Routes = [
  {path: 'fatura', component: FaturaFormComponent },
  {path: 'fatura/:id', component: FaturaFormComponent},
  {path: 'faturas', component: FaturaListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
