import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Fatura } from '../fatura-form/fatura';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fatura-list',
  templateUrl: './fatura-list.component.html',
  styleUrls: ['./fatura-list.component.css']
})
export class FaturaListComponent implements OnInit {

   faturas: Fatura[];
   public fatura: Fatura;

  constructor(private httpCliente: HttpClient, private activatedRoute: ActivatedRoute, public router: Router) {
    this.httpCliente.get<Fatura[]>('http://localhost:3000/api/faturas').toPromise().then(res => {
     this.faturas = res;
    })
    .catch(err => {
       console.log(err);
    });
  }
  editar(idfatura) {
    this.router.navigate(['/fatura/' + idfatura]);
  }
   excluir(idfatura){
    console.log(idfatura);
    this.httpCliente.delete<Fatura>('http://localhost:3000/api/delete/' + idfatura).toPromise().then(res => {
      this.fatura = res;
     })
     .catch(err => {
        console.log(err);
     });
    window.location.reload();
  }
  ngOnInit() {
  }

}
