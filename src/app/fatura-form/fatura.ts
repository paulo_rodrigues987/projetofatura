export class Fatura{
  idfatura: number;
  idusuario: number;
  nome_empresa: string;
  valor: number;
  data_vencimento: string;
  pagou: string;
}
