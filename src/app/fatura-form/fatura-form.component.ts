import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Fatura } from './fatura';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-fatura-form',
  templateUrl: './fatura-form.component.html',
  styleUrls: ['./fatura-form.component.css']
})

export class FaturaFormComponent implements OnInit {

  public fatura: Fatura;

  constructor( private httpCliente: HttpClient, private activatedRoute: ActivatedRoute, public router: Router) {
    this.fatura = new Fatura();
  }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    if (id !== null ) {
    this.httpCliente.get<Fatura>('http://localhost:3000/api/faturas/form/' + id).toPromise().then(res => {
      this.fatura = res;
     })
     .catch(err => {
        console.log(err);
     });
    }

  }

  atualizar() {
    this.httpCliente.post<Fatura>('http://localhost:3000/api/update/', this.fatura).toPromise().then(res => {
      this.fatura = res;
     })
     .catch(err => {
        console.log(err);
     });
    window.location.href = '/faturas';
  }
  cancelar() {
    this.router.navigate(['/faturas']);
  }
}
