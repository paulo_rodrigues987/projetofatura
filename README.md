# CRUD com Node.js, SQLite e Angular

### Update: 10.05.2019

> Este projeto foi desenvolvido durante os testes para desenvolvedor backend.

### Pré-requisitos

- **Node.js** v10.15.3;
- **Nodemon** v1.19.0;
- **Angular** v7.2.0;
- **SQLite3** v4.0.2;
- **Express** v4.16.3;

### Instalação

1. Faça o clone do repositório e no terminal navegue até a pasta;
2. Instale as dependências do projeto com `npm install`;
3. Rode o serviço com `npm start`;
4. Não é necessário criação de arquivo database. O projeto já possui o mesmo e consequentemente realizndo a criação do mesmo;
5. Realize o clone do repositório entitulado **projetofatura** (Responsável pelo Front-end) e no terminal navegue até a pasta;
6. Instale as dependências do projeto com 'npm install';
7. Rode o servidor com `ng serve`;
8. Abra o navegador e acesse o endereço `http://localhost:4200/`;
9. Os endpoints do serviço estarão disponiveis em `http://localhost:3000/`;

### Sugestão

Utilize o Postman para testar suas chamadas. [https://www.getpostman.com/](https://www.getpostman.com/).

### Rotas Criadas no serviço node.js

1. /api/faturas(get) => Realiza a consulta de todas as faturas no db;
2. /api/faturas/form/:id(get) => Realiza a consulta da fatura a ser passada como parametro (ID);
3. /api/update/:id(post) => Realiza a atualização da fatura que foi passada como parametro (ID);
4. /api/update/(post) => Para facilitar manutenção e evitar codificação duplicada no serviço de front-end foi implementado para que o metodo em questão valide a id passada, caso a mesma seja nula o mesmo trata como inserção. Inserindo um novo registro de fatura.
5. /api/delete/:id => Realiza a exclusão da fatura que foi passada como parametro

### Versionalização

- Para melhor organização seguiremos as diretrizes da Versionalização semântica -  1.0.0

## Autor
Paulo César Queiroz Rodrigues
Email: <paulo.rodrigues@icloud.com>,<pcqrodrigues@gmail.com>

### License

The MIT License (MIT)

> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.